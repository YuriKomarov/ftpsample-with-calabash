# tabBar navigation
Then (/^I (?:press|touch) the (url_get|get|list|put|create_dir) tab$/) do |tab|
  wait_for_elements_exist('tabBarButton')
    case tab
      when 'url_get'
        index = 0
      when 'get'
        index = 1
      when 'list'
        index = 2
      when 'put'
        index = 3
      when 'create_dir'
        index = 4
      end
    touch("tabBarButton index:#{index}")
    #expected_view = "#{tab} page"
    #wait_for_elements_exist("view marked:'#{expected_view}'")
  end
