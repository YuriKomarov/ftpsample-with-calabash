  Feature: FTPSample

  Scenario: FTPSample Scenario

  ###################################

  Given I wait for "Tap Get to start getting" to appear

  Then I clear text field number 1
  Then I press "Get"
  Then I wait for "Invalid URL" to appear
  Then I wait

  Then I clear text field number 1
  Then I enter "https://vk.cc/6mZuLE" into text field number 1
  Then I press done
  Then I press "Get"
  Then I wait for "Can only get images" to appear
  Then I wait

  Then I clear text field number 1
  Then I enter "https://pp.userapi.com/c625219/v625219916/eb3/Cz4SQ6JaT6c.jpg" into text field number 1
  Then I press done
  Then I press "Get"
  Then I wait for "GET succeeded" to appear
  Then I wait

  ###################################

  Then I touch the get tab
  Then I wait for "Tap Get to start getting" to appear

  Then I clear text field number 1
  Then I press "Get"
  Then I wait for "Invalid URL" to appear
  Then I wait

  Then I clear text field number 1
  Then I enter "ftp://localhost/1.j" into text field number 1
  Then I press done
  Then I press "Get"
  Then I wait for "Stream open error" to appear
  Then I wait

  Then I clear text field number 1
  Then I enter "ftp://localhost/TestImage1.png" into text field number 1
  Then I press done
  Then I press "Get"
  Then I wait for "GET succeeded" to appear
  Then I wait


  ###################################

  Then I touch the list tab

  Then I clear text field number 1
  Then I press "List"
  Then I wait for "Invalid URL" to appear
  Then I wait

  Then I clear text field number 1
  Then I enter "ftp://localhost/1.j" into text field number 1
  Then I press done
  Then I press "List"
  Then I wait for "Stream open error" to appear

  Then I clear text field number 1
  Then I enter "ftp://localhost/" into text field number 1
  Then I press done
  Then I press "List"
  Then I wait for "List succeeded" to appear

  ##################################

  Then I touch the put tab
  Then I wait for "Tap a picture to start the put" to appear

  Then I clear text field number 1
  Then I clear text field number 2
  Then I clear text field number 3
  Then I touch the "TestImage1" button
  Then I wait for "Invalid URL" to appear

  Then I clear text field number 1
  Then I clear text field number 2
  Then I clear text field number 3
  Then I enter "ftp://localhost/" into text field number 1
  Then I press done
  Then I touch the "TestImage1" button
  Then I wait for "Stream open error" to appear

  Then I clear text field number 1
  Then I clear text field number 2
  Then I clear text field number 3
  Then I enter "ftp://localhost/" into text field number 1
  Then I press done
  Then I enter "yurikomarov" into text field number 2
  Then I press done
  Then I enter "16791" into text field number 3
  Then I press done
  Then I touch the "TestImage1" button
  Then I wait for "Put succeeded" to appear
  Then I touch the "TestImage2" button
  Then I wait for "Put succeeded" to appear
  Then I touch the "TestImage3" button
  Then I wait for "Put succeeded" to appear
  Then I touch the "TestImage4" button
  Then I wait for "Put succeeded" to appear

  ###################################

  Then I touch the create_dir tab
  Then I wait for "Tap Create to create the directory" to appear

  Then I clear text field number 1
  Then I clear text field number 2
  Then I clear text field number 3
  Then I clear text field number 4
  Then I press "Create"
  Then I wait for "Invalid URL" to appear
  Then I wait

  Then I clear text field number 1
  Then I clear text field number 2
  Then I clear text field number 3
  Then I clear text field number 4
  Then I enter "ftp://localhost/" into text field number 1
  Then I press done
  Then I wait
  Then I press "Create"
  Then I wait for "FTP error 530" to appear
  Then I wait

  Then I clear text field number 1
  Then I clear text field number 2
  Then I clear text field number 3
  Then I clear text field number 4
  Then I enter "ftp://localhost/" into text field number 1
  Then I press done
  Then I wait
  Then I enter "yurikomarov" into text field number 2
  Then I press done
  Then I wait
  Then I press "Create"
  Then I wait for "FTP error 530" to appear
  Then I wait

  Then I clear text field number 1
  Then I clear text field number 2
  Then I clear text field number 3
  Then I clear text field number 4
  Then I enter "ftp://localhost/" into text field number 1
  Then I press done
  Then I wait
  Then I enter "yurikomarov" into text field number 2
  Then I press done
  Then I wait
  Then I enter "16791" into text field number 4
  Then I press done
  Then I wait
  Then I press "Create"
  Then I wait for "FTP error 500" to appear
  Then I wait

  Then I clear text field number 1
  Then I clear text field number 2
  Then I clear text field number 3
  Then I clear text field number 4
  Then I enter "ftp://localhost/" into text field number 1
  Then I press done
  Then I wait
  Then I enter "yurikomarov" into text field number 2
  Then I press done
  Then I wait
  Then I enter "16791" into text field number 4
  Then I press done
  Then I wait
  Then I enter "test_folder" into text field number 3
  Then I press done
  Then I wait
  Then I press "Create"
  Then I wait for "Create succeeded" to appear
  Then I wait

  Then I wait and wait
